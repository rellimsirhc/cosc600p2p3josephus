/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cosc600p2p3josephus;

/**
 * @author Christine Miller-Lane
 * crated 11 Mar 2021
 */
import java.io.*;
import java.util.*;

public class Cosc600p2p3Josephus {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       int num, post;
       Scanner input = new Scanner(System.in);
       System.out.println("How many soldiers? ");
       num = input.nextInt();
      // input.skip(System.lineSeparator());
       String[] names = new String[num];
       
       for (int counter=0; counter < num; counter++) {
           System.out.println("Enter the name of solider "+ (counter+1));
           names[counter]= input.next();  //store names in counter position for array
       } //end for
              
       System.out.println("Soliders are: ");
       for (int counter=0; counter < num; counter++) {
           System.out.println(names[counter]+ " ("+(counter) +")");
       } //end for
       
       System.out.println();
       System.out.println("Which number soldier will be removed first?  ");
       post = input.nextInt();
       
       input.close(); // close scanner
       System.out.println();
       System.out.println("Eliminating Order: ");
       //System.out.print(removeSoldier(post));   

 }  // end public static void main10
       
//////remove items

    // function (takes a  list)
 public void Removal (List<String> arrayList) {  
  //remember what index in that list we're at
  int index =0;
  int count = 0;
  //remember whether this is the item we want to delete.

  //loop until the list is size 1
  while (arrayList.size() !=1){
  //  increment the item we're looking at.
  index = index+1;
  //  increment the delete count we're on
  count = count +1;

    //should we delete? if so, delete!
    // print item we are deleteing
    //  reset delete count
    
      String word = arrayList.get(index);  //get next item
      
    //are we at the end of the list?
    if (count ==3){
        arrayList.remove(index - 1);
        count = 0;
    }       
        // if so, pring last item and reset our index
  }
 }
///////////
       
       
       

    
  /* public static int removeSoldier(int n)
   {
       if (n == 1)
           return 1;
       
       if (n%post == 0)
           return 2 * removeSoldier(n/2) -1;
       else
       return 2 * removeSoldier(((n-1)/2))+1;
   }  //end removeSoldier */
    
    
} // end public class Cosc600p2p3Josephus
